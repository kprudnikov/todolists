'use strict';

module.exports = function (grunt) {
  require('jit-grunt')(grunt, {
    express: 'grunt-express-server',
    nodemon: "grunt-nodemon"
  });

  var port = 15301;
  var script = './bin/www';

  grunt.initConfig({
    express: {
      server: {
        options: {
          port: process.env.PORT || port,
          script: script
        }
      }
    },
    open: {
      server: {
        url: 'http://localhost:<%= express.server.options.port %>'
      }
    },
    nodemon: {
      script: '/',
      options: {
        env: {
          PORT: process.env.PORT || port
        },
        callback: function (nodemon) {
        }
      }
    }
  });

  grunt.registerTask('default', ['express', 'open', 'nodemon']);
};
To-do lists test app
====================

install MySQL before running.
create database 'todolists' for root user

  npm install
  grunt

deploy on Digital Ocean
(see package.json for version)

  install nodejs
  install npm
  install nginx

go to '/etc/nginx/sites-available' and add under 'server' line 

  location / {
      proxy_pass http://localhost:15301/;
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }

run

  service run nginx

add todoapp file to /etc/init.d/

  service run todoapp


additional info:
https://www.digitalocean.com/community/tutorials/how-to-deploy-node-js-applications-using-systemd-and-nginx
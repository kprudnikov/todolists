var Sequelize = require('sequelize');
var sequelize = null;
var model = {};

 if (process.env.HEROKU_POSTGRESQL_ROSE_URL) {

    console.log("+++++++++ URL +++++++++");
    console.log(process.env.HEROKU_POSTGRESQL_ROSE_URL);
    console.log("========= URL !=========");


    // the application is executed on Heroku ... use the postgres database
    sequelize = new Sequelize('postgres://givetxduzbfwsq:Yb6STFB3l_LAuvdTg8sHbdMbTg@ec2-107-21-102-69.compute-1.amazonaws.com:5432/d4ptgklqpshoc1');
    // sequelize = new Sequelize(process.env.HEROKU_POSTGRESQL_ROSE_URL, {
      // dialect:  'postgres',
      // protocol: 'postgres',
      // port:     match[4],
      // host:     match[3],
      // logging:  true //false
    // })
  } else {
    // the application is executed on the local machine ... use mysql

    sequelize = new Sequelize('todolists', 'root', null, {
      dialectOptions: {
        charset: 'utf8',
      }
    })
  }

var TaskList = sequelize.define('list', {
  title: {
    type: Sequelize.STRING,
    field: 'title'
  }
},
{
  charset: 'utf8'
});

var Task = sequelize.define('task', {
  todo: {
    type: Sequelize.STRING,
    field: 'todo'
  },
  done: {
    type: Sequelize.BOOLEAN,
    field: 'done'
  }
},
{
  charset: 'utf8'
});

TaskList.hasMany(Task, {as: 'Tasks'});

model.task = Task;
model.tasklist = TaskList;

sequelize.sync();
module.exports = model;
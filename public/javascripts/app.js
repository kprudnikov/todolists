var app = /**
* todolist Module
*
* Description
*/
angular.module('todolist', ['ngRoute']).
config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
  'user strict';

  var routeConfig = {
    controller: 'todoCtrl',
    templateUrl: 'templates/index.html'
  };

  $routeProvider
    .when('/', routeConfig)
    .otherwise({
      redirectTo: '/'
    });

  $httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '1728000';
  $httpProvider.defaults.headers.common['Cache-Control'] = 'max-age=1728000';
}])
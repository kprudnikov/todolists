app.controller('todoCtrl', ['$scope', '$http', '$timeout', function($scope, $http, $timeout){
  $scope.lists = [];

  $scope.tobeRemoved = [];
  $scope.serverError = false;
  var loading = false;
  var saveTimeout;

  function saveMultipleTasks () {
    return $http.post('/updateTasks', {
      tasks: Array.prototype.slice.call(arguments)
    });
  }

  $scope.toggleElement = function (index, array) {
    if(array.indexOf(index) > -1) {
      array.splice(array.indexOf(index), 1);
    } else {
      array.push(index);
    }
  };

  $scope.removeLists = function (index) {

    $http.post('/deleteTaskLists', {
      data: $scope.tobeRemoved.map(function (el) {
        return $scope.lists[el].id;
      })
    }).success(function() {
      if ($scope.serverError) {
        $scope.serverError = false;
      }
      $scope.tobeRemoved.forEach(function (el) {
        $scope.lists[el] = undefined;
      });

      $scope.lists = $scope.lists.filter(function(el, ind, ar){
        return el !== undefined;
      });

      $scope.tobeRemoved = [];
    });
  };

  $scope.cancelRemove = function () {
    $scope.tobeRemoved = [];
  };

  $scope.addList = function () {
    $scope.loading = true;

    $http.post('/createTaskList')
      .success(function (data) {
        if ($scope.serverError) {
          $scope.serverError = false;
        };
        $scope.lists.unshift(data);
      })
      .finally(function () {
        $scope.loading = false;
      });
  };

  $scope.postTitle = function (list) {
    $http.post('/saveTitle', {
      id: list.id,
      title: list.title
    }).
    success(function () {
      if ($scope.serverError) {
        $scope.serverError = false;
      };
    })
  }

  $scope.addTask = function (list) {
    if(!list.tasks) {
      list.tasks = [];
    }
    list.tasks.push({});
  };

  $scope.saveTask = function (task, list, callback) {
    callback();
    $http.post('/createTask', {
      listId: list.id,
      taskid: task.id,
      todo: task.todo,
      done: task.done
    }).success( function (data, status) {
      if ($scope.serverError) {
        $scope.serverError = false;
      }
      task.id = data.id;
    })
  };

  $scope.disableEditMode = function (childScope) {
    return function () {
      childScope.inEditMode = false;
    }
  }

  $scope.toggleDone = function (task){
    return function (){
      task.done = !task.done;      
    }
  };

  $scope.toggleAddToRemove = function (index) {
    $scope.toggleElement(index, $scope.tobeRemoved);
  };

  $scope.deleteTask = function (list, index) {
    if(list.tasks[index].id){
      $http.post('/deleteTask', {
        id: list.tasks[index].id
      }).success(function () {
        if ($scope.serverError) {
          $scope.serverError = false;
        }
        list.tasks.splice(index, 1);
      });
    } else {
        list.tasks.splice(index, 1);
    }
  };

  $scope.moveTaskUp = function (list, index) {
    if (index) {
      var interchangeTask = list[index].todo;
      list[index].todo = list[index-1].todo;
      list[index-1].todo = interchangeTask;
      saveMultipleTasks(list[index], list[index-1]).
      error(function () {
        list[index-1].todo = list[index].todo;
        list[index].todo = interchangeTask;
        $scope.serverError = true;
      });
    }
  };

  $scope.moveTaskDown = function (list, index) {
    console.log(list[index]);
    console.log(list[index+1]);
    if (index < list.length-1) {
      var interchangeTask = list[index].todo;
      list[index].todo = list[index+1].todo;
      list[index+1].todo = interchangeTask;
      saveMultipleTasks(list[index], list[index+1]).
      error(function () {
        list[index+1].todo = list[index].todo;
        list[index].todo = interchangeTask;
        $scope.serverError = true;
      });
    }
  };

  $scope.getAll = function () {
    $http.get('/getAll')
    .success(function (data) {
      if ($scope.serverError) {
        $scope.serverError = false;
      }
      $scope.lists = data.reverse();
    })
  }

  $scope.getAll();

}]);
app.directive('ngToggleEdit', function($parse){
  // Runs during compile
  return {
    name: 'ngToggleEdit',
    // priority: 1,
    // terminal: true,
    scope: true,
    controller: function($scope, $element, $attrs, $transclude) {
      if ($scope.task.id) {
        $scope.inEditMode = false;
      } else {
        $scope.inEditMode = true;
      };

      $element.on('click', function () {
        $element.find('input')[0].focus();
      });
      
    },
    restrict: 'A' // E = Element, A = Attribute, C = Class, M = Comment
  };
});
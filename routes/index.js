var express = require('express');
var router = express.Router();
var app = require('../app');
var auth = require('../auth');
var model = require('../model');

var Task = model.task;
var TaskList = model.tasklist;


router.get('/', function(req, res, next) {
  if(req.session.passport.user === undefined) {
    res.redirect('/login');
  } else {
    res.render('index');
  }
});

router.get('/app', function(req, res, next) {
  if(req.session.passport.user === undefined) {
    res.redirect('/login');
  } else {
    res.render('index');
  }
});

router.get('/login',  function (req, res, next) {
  res.render('login');
});

router.post('/login', auth.authenticate('local', {
  failureRedirect: '/login',
  successRedirect: '/app'
}));

router.post('/createTaskList', function (req, res, next) {
  TaskList.create().success(function (list) {
    res.send(list);
  })
});

router.post('/saveTitle', function (req, res, next) {
  TaskList.find(req.body.id).success(function (list) {
    list.setDataValue('title', req.body.title);
    list.save();
    res.sendStatus(200);
  })
})

router.post('/deleteTaskLists', function (req, res, next) {
  req.body.data.forEach( function(id) {
    TaskList.find(id).on('success', function (list){
      Task.destroy({where: {
        listId: list.id
      }})
      .then(function () {
        list.destroy().
        then(function () {
          res.send(200);
        });
      });
    });
  });
});

router.get('/getAll', function (req, res, next) {
  TaskList.findAll().on('success', function(lists){

    var data = [];
    var loaded = 0;
    lists.forEach(function (list, index) {
      list.getTasks()
      .then(function (tasks) {
        data[index] = {};
        data[index].id = list.id;
        data[index].title = list.title;
        data[index].tasks = tasks;

        loaded += 1;
        if(loaded === lists.length){
          res.send(data);
        }
      })
    });
  });
});

router.post('/createTask', function (req, res, next) {
  if(req.body.taskid) {
    Task.find(req.body.taskid).
    on('success', function (task) {
      task.setDataValue('done', req.body.done);
      task.setDataValue('todo', req.body.todo);
      task.save();
      res.send({id: req.body.taskid});
    });
  } else {
    Task.create({todo: req.body.todo, done: req.body.done})
    .on('success', function (task) {
      TaskList.find(req.body.listId).on('success', function (list) {
        list.addTask(task).then(function () {
          res.send({id: task.id});
        });
      });
    });
  }
});

router.post('/updateTasks', function (req, res, next) {
  var loaded = 0;
  req.body.tasks.forEach(function(task){
    Task.find(task.id).on('success', function (detectedTask) {
      detectedTask.setDataValue('todo', task.todo);
      detectedTask.save().on('success', function () {
        loaded += 1;
        if (loaded === req.body.tasks.length) {
          res.sendStatus(200);
        }
      })
    })
  });

});

router.post('/deleteTask', function (req, res, next) {
  Task.find(req.body.id)
  .success(function (task) {
    task.destroy().
    then(function () {
      res.send(200);
    });
  });
});

router.get('/logout', function (req, res) {
  req.logout();
  console.log("LOGOUT BITCHES!!!");
  // res.sendStatus(200);
  res.redirect('/');
});

// sequelize.query("SET NAMES utf8;")
// sync creates tables if they don't exist
module.exports = router;